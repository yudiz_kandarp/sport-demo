export function timeFromNow(itemDate) {
	const d = new Date()
	let months = [
		'',
		'a',
		'two',
		'three',
		'four',
		'five',
		'six',
		'seven',
		'eight',
		'nine',
		'ten',
		'eleven',
	]

	if (d.getFullYear() - new Date(itemDate).getFullYear() >= 1) {
		let year = months[d.getFullYear() - new Date(itemDate).getFullYear()]
		return `${year} ${year == 'a' ? 'year ago' : 'years ago'}`
	} else if (d.getMonth() - new Date(itemDate).getMonth() >= 1) {
		let Month = months[d.getMonth() - new Date(itemDate).getMonth()]
		return `${Month} ${Month == 'a' ? 'month ago' : 'months ago'}`
	} else {
		return 'recently added'
	}
}
