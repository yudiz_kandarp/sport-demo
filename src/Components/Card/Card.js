import React from 'react'
import PropTypes from 'prop-types'
import { timeFromNow } from '../../Utils/helper'

function Card({ item }) {
	const timeline = timeFromNow(item.dUpdatedAt)

	return (
		<>
			<div className='card'>
				<div className='card_img_container'>
					<div
						className='card_img'
						style={{
							backgroundImage: `url(${item.sImage})`,
						}}
					/>
				</div>
				<div className='card_details_container'>
					<h5>
						<a href='/'>{item.sTitle}</a>
					</h5>
					<p className='description'>
						{item.sDescription.slice(0, 164) + '...'}
					</p>
					<div className='card_info'>
						<div className='timeline'>
							<a href='/' className='sport_info'>
								{item.iId.sFirstName + ' ' + item.iId.sLastName}
							</a>
							<p className='time'>{timeline} </p>
						</div>
						<div className='view-comment'>
							<div className='comment'>
								<p
									className='icon'
									style={{
										backgroundImage: `url("comment.svg")`,
									}}
								></p>
								<p>{item.nCommentsCount}</p>
							</div>
							<div className='view'>
								<p
									className='icon'
									style={{
										backgroundImage: `url("eye.svg")`,
									}}
								></p>
								<p>{item.nViewCounts}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}
Card.propTypes = {
	item: PropTypes.object,
}
export default Card
