import React, { useEffect, useState } from 'react'
import { Card } from '../Components'
import jsonData from '../data/sports-info.json'

function Home() {
	const [data, setData] = useState()
	useEffect(() => {
		if (jsonData.status === 200) {
			setData(jsonData)
		}
	}, [])
	return (
		<>
			<section>
				<div className='card_container'>
					{!data && 'No Data Found...'}
					{data?.data.map((item) => (
						<Card key={item._id} item={item} />
					))}
				</div>
			</section>
		</>
	)
}

export default Home
